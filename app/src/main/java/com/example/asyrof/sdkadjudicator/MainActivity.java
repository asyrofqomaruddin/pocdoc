package com.example.asyrof.sdkadjudicator;

import android.app.*;
import android.content.Intent;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.qiscus.sdk.Qiscus;
import com.qiscus.sdk.data.model.QiscusAccount;
import com.qiscus.sdk.data.model.QiscusChatRoom;
import com.qiscus.sdk.data.remote.QiscusApi;
import com.qiscus.sdk.presenter.QiscusChatPresenter;
import com.qiscus.sdk.ui.QiscusGroupChatActivity;
import com.qiscus.sdk.util.QiscusRxExecutor;

import java.io.IOException;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import retrofit2.HttpException;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private QiscusChatPresenter qiscusChatPresenter;
    static final String BUNDER_EMAIL = "bunder2@qiscus.com";
    static final String ASYROF_EMAIL = "asyrof2@qiscus.com";
    static final String PASSWORD = "password";
    private static final String TAG = "MainActivity";
    static final String ROOM_ID = "ROOMSDK";
    private Button startPdfButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //qiscusChatPresenter = new QiscusChatPresenter(this, qiscusChatRoom);
        //showchat();
        startPdfButton = (Button) findViewById(R.id.startPdf);
        startPdfButton.setOnClickListener(this);

    }

    public void groupChat(View view) {
        Qiscus.buildGroupChatRoom("CheckBro", Arrays.asList(ASYROF_EMAIL, BUNDER_EMAIL))
                .withAvatar("http://avatar.url.com/group.jpg")
                .build(new Qiscus.ChatBuilderListener() {
                    @Override
                    public void onSuccess(QiscusChatRoom qiscusChatRoom) {
                        Log.d("Room id", "onSuccess: "+ qiscusChatRoom.getId());
                        startActivity(QiscusGroupChatActivity.generateIntent(MainActivity.this, qiscusChatRoom));
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        if (throwable instanceof HttpException) { //Error response from server
                            HttpException e = (HttpException) throwable;
                            try {
                                String errorMessage = e.response().errorBody().string();
                                Log.e(TAG, errorMessage);
                                showError(errorMessage);
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                        } else if (throwable instanceof IOException) { //Error from network
                            showError("Can not connect to qiscus server!");
                        } else { //Unknown error
                            showError("Unexpected error!");
                        }
                    }
                });
    }


    public void byIdRoom(View view) {

        QiscusApi.getInstance()
                .getChatRoom(47756)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(qiscusChatRoom -> QiscusGroupChatActivity.generateIntent(this, qiscusChatRoom))
                .subscribe(this::startActivity, throwable -> {
                    if (throwable instanceof HttpException) { //Error response from server
                        HttpException e = (HttpException) throwable;
                        try {
                            String errorMessage = e.response().errorBody().string();
                            Log.e(TAG, errorMessage);
                            showError(errorMessage);
                        } catch (IOException e1) {
                            e1.printStackTrace();
                        }
                    } else if (throwable instanceof IOException) { //Error from network
                        showError("Can not connect to qiscus server!");
                    } else { //Unknown error
                        showError("Unexpected error!");
                    }
                });

    }

    public void byUniqueId(View view) {
        Qiscus.buildGroupChatRoomWith(ROOM_ID)
                .withName("RoomName")
                .withAvatar("http://avatar.url.com/group.jpg")
                .build(new Qiscus.ChatBuilderListener() {
                    @Override
                    public void onSuccess(QiscusChatRoom qiscusChatRoom) {
                        startActivity(QiscusGroupChatActivity.generateIntent(MainActivity.this, qiscusChatRoom));
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        if (throwable instanceof HttpException) { //Error response from server
                            HttpException e = (HttpException) throwable;
                            try {
                                String errorMessage = e.response().errorBody().string();
                                Log.e(TAG, errorMessage);
                                showError(errorMessage);
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                        } else if (throwable instanceof IOException) { //Error from network
                            showError("Can not connect to qiscus server!");
                        } else { //Unknown error
                            showError("Unexpected error!");
                        }
                    }
                });
    }

    public void setUserBunder(View view) {
        Log.d("Bunder", "Check");
        setuser(BUNDER_EMAIL, "bunder");
    }

    public void setUserAsyrof(View view) {
        Log.d("Asyrof", "Check");
        setuser(ASYROF_EMAIL, "Asyrof");
    }

    private void setuser(String email, String name) {
        String avatar;
        if (email.equals(ASYROF_EMAIL))
        {
            avatar = "https://upload.wikimedia.org/wikipedia/en/thumb/b/b4/Donald_Duck.svg/618px-Donald_Duck.svg.png";
        }
        else
        {
            avatar = "https://upload.wikimedia.org/wikipedia/en/d/d4/Mickey_Mouse.png";
        }
        Qiscus.setUser(email, PASSWORD)
                .withUsername(name)
                .withAvatarUrl(avatar)
                .save(new Qiscus.SetUserListener() {
                    @Override
                    public void onSuccess(QiscusAccount qiscusAccount) {
                        Log.d("tes", qiscusAccount.toString());
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        if (throwable instanceof HttpException) { //Error response from server
                            HttpException e = (HttpException) throwable;
                            try {
                                String errorMessage = e.response().errorBody().string();
                                Log.e(TAG, errorMessage);
                                showError(errorMessage);
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                        } else if (throwable instanceof IOException) { //Error from network
                            showError("Can not connect to qiscus server!");
                        } else { //Unknown error
                            showError("Unexpected error!");
                        }
                    }
                });
    }


    public void clearUser(View view) {
        Qiscus.clearUser();
    }


    private void showError(String errorMessage) {
        Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show();
    }

    private void startPdf(){
        startActivity(new Intent(this,FileChatActivity.class));
    }

    public void roomList(View view) {
        QiscusApi.getInstance().getChatRooms(1, 20, true)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(qiscusChatRooms -> {
                    Log.d("RESULT",qiscusChatRooms.toString());
                    //Toast.makeText(this, qiscusChatRooms.toString(), Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(MainActivity.this, RoomListActivity.class);
                    //intent.putExtra("ROOM_LIST", (Parcelable) qiscusChatRooms);
                    //intent.putExtra("key","value");
                    startActivity(intent);
                }, throwable -> {
                    //Something went wrong
                });
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.startPdf) {
            startPdf();
        }
    }


}
