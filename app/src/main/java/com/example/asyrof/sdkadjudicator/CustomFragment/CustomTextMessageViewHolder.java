package com.example.asyrof.sdkadjudicator.CustomFragment;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.asyrof.sdkadjudicator.R;
import com.qiscus.nirmana.Nirmana;
import com.qiscus.sdk.data.model.QiscusComment;
import com.qiscus.sdk.ui.adapter.OnItemClickListener;
import com.qiscus.sdk.ui.adapter.OnLongItemClickListener;
import com.qiscus.sdk.ui.adapter.viewholder.QiscusBaseMessageViewHolder;
import com.qiscus.sdk.ui.adapter.viewholder.QiscusBaseTextMessageViewHolder;

/**
 * Created by asyrof on 12/12/17.
 */

public class CustomTextMessageViewHolder extends QiscusBaseTextMessageViewHolder {

    public CustomTextMessageViewHolder(View itemView, OnItemClickListener itemClickListener,
                                       OnLongItemClickListener longItemClickListener) {
        super(itemView, itemClickListener, longItemClickListener);

    }

    @NonNull
    @Override
    protected TextView getMessageTextView(View itemView) {
        return (TextView) itemView.findViewById(R.id.contents);
    }

    @Nullable
    @Override
    protected ImageView getFirstMessageBubbleIndicatorView(View itemView) {
        return (ImageView) itemView.findViewById(R.id.bubble);
    }

    @NonNull
    @Override
    protected View getMessageBubbleView(View itemView) {
        return itemView.findViewById(R.id.message);
    }

    @Nullable
    @Override
    protected TextView getDateView(View itemView) {
        return (TextView) itemView.findViewById(R.id.date);
    }

    @Nullable
    @Override
    protected TextView getTimeView(View itemView) {
        return (TextView) itemView.findViewById(R.id.time);
    }

    @Nullable
    @Override
    protected ImageView getMessageStateIndicatorView(View itemView) {
        return (ImageView) itemView.findViewById(R.id.icon_read);
    }

    @Nullable
    @Override
    protected ImageView getAvatarView(View itemView) {
        return (ImageView) itemView.findViewById(R.id.avatar);
    }

    @Nullable
    @Override
    protected TextView getSenderNameView(View itemView) {
        return (TextView) itemView.findViewById(R.id.name);
    }



    @Override
    protected void setUpColor() {
        this.setMessageFromMe(false);
        /*if (messageFromMe) {
            messageBubbleView.setBackgroundColor(rightBubbleColor);
            if (firstMessageBubbleIndicatorView != null) {
                firstMessageBubbleIndicatorView.setColorFilter(rightBubbleColor);
            }

        } else {
            messageBubbleView.setBackgroundColor(leftBubbleColor);
            if (firstMessageBubbleIndicatorView != null) {
                firstMessageBubbleIndicatorView.setColorFilter(leftBubbleColor);
            }
        }*/

        if (dateView != null) {
            dateView.setTextColor(dateColor);
        }
    }

}
