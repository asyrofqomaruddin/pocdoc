package com.example.asyrof.sdkadjudicator.CustomFragment;

import android.os.Bundle;

import com.example.asyrof.sdkadjudicator.R;
import com.qiscus.sdk.data.model.QiscusChatRoom;
import com.qiscus.sdk.ui.adapter.QiscusChatAdapter;
import com.qiscus.sdk.ui.fragment.QiscusChatFragment;

/**
 * Created by asyrof on 12/12/17.
 */

public class CustomChatFragment extends QiscusChatFragment {
    public static CustomChatFragment newInstance(QiscusChatRoom qiscusChatRoom) {
        CustomChatFragment fragment = new CustomChatFragment();
        Bundle bundle = new Bundle();
        qiscusChatRoom.setGroup(true);
        bundle.putParcelable(CHAT_ROOM_DATA, qiscusChatRoom);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected QiscusChatAdapter onCreateChatAdapter() {
        return new CustomChatAdapter(getActivity(), qiscusChatRoom.isGroup());
    }

    @Override
    protected int getResourceLayout() {
        return R.layout.fragment_sticker_chat;
    }

}