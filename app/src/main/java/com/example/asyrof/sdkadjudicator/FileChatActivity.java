package com.example.asyrof.sdkadjudicator;

import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.asyrof.sdkadjudicator.CustomFragment.CustomChatFragment;
import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.scroll.DefaultScrollHandle;
import com.qiscus.sdk.data.model.QiscusChatRoom;
import com.qiscus.sdk.data.remote.QiscusApi;
import com.qiscus.sdk.util.QiscusRxExecutor;

import java.util.List;

public class FileChatActivity extends AppCompatActivity implements View.OnClickListener{
    private static final String SAMPLE_FILE = "pdf.pdf";
    private Button startChatButton;
    private PDFView mypdfview;
    private List<QiscusChatRoom> list;
    private View dimmer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadRooms();
        setContentView(R.layout.activity_file_chat);
        startChatButton = (Button) findViewById(R.id.startChatFile);
        startChatButton.setOnClickListener(this);
        mypdfview =(PDFView)findViewById(R.id.fileViewer);
        displayFromAsset(SAMPLE_FILE);
        dimmer = findViewById(R.id.dimmer);

    }


    private void displayFromAsset(String assetFileName) {
        String pdfFileName = assetFileName;

        mypdfview.fromAsset(pdfFileName)
                .defaultPage(2)
                .enableSwipe(true)
                .swipeHorizontal(false)
                .enableAnnotationRendering(true)
                .scrollHandle(new DefaultScrollHandle(this))
                .load();
    }
    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.startChatFile) {
            //dimmer.getBackground().setAlpha((int) 0.5);
            dimmer.setVisibility(View.VISIBLE);
            startChat();
        }
    }


    private void loadRooms() {
        QiscusRxExecutor.execute(QiscusApi.getInstance().getChatRooms(1, 100, true), new QiscusRxExecutor.Listener<List<QiscusChatRoom>>() {
            @Override
            public void onSuccess(List<QiscusChatRoom> result) {
                list = result;
            }

            @Override
            public void onError(Throwable throwable) {
                throwable.printStackTrace();
            }
        });
    }

    private void startChat() {
        QiscusChatRoom room = list.get(0);
        CustomChatFragment qiscusCustomChatFragment = CustomChatFragment.newInstance(room);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.slide_in_up, R.anim.slide_out_up);
        ft.replace(R.id.fragment_container,qiscusCustomChatFragment).commit();
        /*Qiscus.buildChatFragmentWith("asyrof2@qiscus.com").build(new Qiscus.ChatFragmentBuilderListener() {
            @Override
            public void onSuccess(QiscusChatFragment qiscusChatFragment) {
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_container,qiscusChatFragment).commit();
            }

            @Override
            public void onError(Throwable throwable) {

            }
        });*/

    }

    private void showError(String error) {
        Toast.makeText(this,error,Toast.LENGTH_SHORT).show();
    }
}


