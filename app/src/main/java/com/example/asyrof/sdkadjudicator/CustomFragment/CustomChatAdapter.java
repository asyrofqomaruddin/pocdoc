package com.example.asyrof.sdkadjudicator.CustomFragment;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.view.ViewGroup;

import com.example.asyrof.sdkadjudicator.R;
import com.qiscus.sdk.data.model.QiscusComment;
import com.qiscus.sdk.ui.adapter.QiscusChatAdapter;
import com.qiscus.sdk.ui.adapter.viewholder.QiscusBaseMessageViewHolder;

/**
 * Created by asyrof on 12/12/17.
 */

public class CustomChatAdapter extends QiscusChatAdapter {
    private final int TYPE_MESSAGE_ME = 2333;
    private final int TYPE_MESSAGE_OTHER = 2334;
    private final int TYPE_MESSAGE_EMPTY = 2335;

    public CustomChatAdapter(Context context, boolean groupChat) {
        super(context, groupChat);
    }

    @Override
    protected int getItemViewTypeMyMessage(QiscusComment qiscusComment, int position) {
        if (qiscusComment.getMessage().equalsIgnoreCase("EMPTY")) {
            return TYPE_MESSAGE_EMPTY;
        } else if (qiscusComment.getType() == QiscusComment.Type.TEXT) {
            return TYPE_MESSAGE_OTHER;
        }
        return super.getItemViewTypeMyMessage(qiscusComment, position);
    }

    @Override
    protected int getItemViewTypeOthersMessage(QiscusComment qiscusComment, int position) {
        if (qiscusComment.getMessage().equalsIgnoreCase("EMPTY")) {
            return TYPE_MESSAGE_EMPTY;
        } else if (qiscusComment.getType() == QiscusComment.Type.TEXT) {
            return TYPE_MESSAGE_OTHER;
        }
        return super.getItemViewTypeOthersMessage(qiscusComment, position);
    }

    @Override
    protected int getItemResourceLayout(int viewType) {
        switch (viewType) {
            case TYPE_MESSAGE_ME:
                return R.layout.item_message_text;
            case TYPE_MESSAGE_OTHER:
                return R.layout.item_message_text;
            case TYPE_MESSAGE_EMPTY:
                return R.layout.item_message_empty_system;
            default:
                return super.getItemResourceLayout(viewType);
        }
    }

    @Override
    public QiscusBaseMessageViewHolder<QiscusComment> onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case TYPE_MESSAGE_ME:
                return new CustomTextMessageViewHolder(getView(parent, viewType), itemClickListener, longItemClickListener);
            case TYPE_MESSAGE_OTHER:
                return new CustomTextMessageViewHolder(getView(parent, viewType), itemClickListener, longItemClickListener);
            case TYPE_MESSAGE_EMPTY:
                //return new EmptySystemEventViewHolder(getView(parent, viewType), itemClickListener, longItemClickListener);
            default:
                return super.onCreateViewHolder(parent, viewType);
        }
    }
}

