package com.example.asyrof.sdkadjudicator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;

import com.qiscus.sdk.data.model.QiscusChatRoom;

import java.util.ArrayList;
import java.util.List;

public class RoomListActivity extends AppCompatActivity {
    ArrayList<RoomModel> RoomModels;
    ListView listView;
    List<QiscusChatRoom> qiscusChatRooms;
    private static CustomAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room_list);
        ListView listView = (ListView) findViewById(R.id.list);
        RoomModels= new ArrayList<>();
        Log.d("BEFORE PARCEL","HERE");
        //qiscusChatRooms = (List<QiscusChatRoom>) getIntent().getParcelableExtra("ROOM_LIST");
        //Log.d("RESULT PARCEL",qiscusChatRooms.toString());
        RoomModels.add(new RoomModel("Apple Pie", "Android 1.0", "1","September 23, 2008"));
        RoomModels.add(new RoomModel("Banana Bread", "Android 1.1", "2","February 9, 2009"));
        RoomModels.add(new RoomModel("Cupcake", "Android 1.5", "3","April 27, 2009"));
        RoomModels.add(new RoomModel("Donut","Android 1.6","4","September 15, 2009"));
        RoomModels.add(new RoomModel("Eclair", "Android 2.0", "5","October 26, 2009"));
        RoomModels.add(new RoomModel("Froyo", "Android 2.2", "8","May 20, 2010"));
        RoomModels.add(new RoomModel("Gingerbread", "Android 2.3", "9","December 6, 2010"));
        RoomModels.add(new RoomModel("Honeycomb","Android 3.0","11","February 22, 2011"));
        RoomModels.add(new RoomModel("Ice Cream Sandwich", "Android 4.0", "14","October 18, 2011"));
        RoomModels.add(new RoomModel("Jelly Bean", "Android 4.2", "16","July 9, 2012"));
        RoomModels.add(new RoomModel("Kitkat", "Android 4.4", "19","October 31, 2013"));
        RoomModels.add(new RoomModel("Lollipop","Android 5.0","21","November 12, 2014"));
        RoomModels.add(new RoomModel("Marshmallow", "Android 6.0", "23","October 5, 2015"));

        adapter= new CustomAdapter(RoomModels,getApplicationContext());

        listView.setAdapter(adapter);
    }
}
