package com.example.asyrof.sdkadjudicator;

import android.app.Application;

import com.qiscus.sdk.Qiscus;

/**
 * Created by asyrof on 30/10/17.
 */

public class QiscusApp  extends Application{
    @Override
    public void onCreate() {
        super.onCreate();
        Qiscus.init(this, "buddygo-stag");
    }
}
